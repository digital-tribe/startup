const app = {
    init() {
        this.setCookieNotice()
        console.log("init")
    },
    setCookieNotice() {
        if(localStorage.getItem("cookie-policy")) return
        cookie_notice.style.display = "block"
        cookie_button.addEventListener("click", this.cookieNoticeEvent.bind(this))
    },
    cookieNoticeEvent(e){
        cookie_notice.style.display = "none"
        localStorage.setItem("cookie-policy",Date.now())
    }
}

app.init()
